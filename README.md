# Raku binding to original Keccak256-2011

Fast original Keccak256-2011 computation using NativeCall to C.

## License

Module `Node::Ethereum::Keccak256::Native` is free and opensource software, so you can redistribute it and/or modify it under the terms of the [The Artistic License 2.0](https://opensource.org/licenses/Artistic-2.0).

## Credits

1. [https://medium.com/@ConsenSys/are-you-really-using-sha-3-or-old-code-c5df31ad2b0](https://medium.com/@ConsenSys/are-you-really-using-sha-3-or-old-code-c5df31ad2b0)
2. [https://github.com/epicblockchain/whitepapers/blob/master/ETC/SHA3/SHA-3_Transition_Whitepaper_Impact_on_Ethereum_Classic_Mining_Hardware_and_Network_Security.md](https://github.com/epicblockchain/whitepapers/blob/master/ETC/SHA3/SHA-3_Transition_Whitepaper_Impact_on_Ethereum_Classic_Mining_Hardware_and_Network_Security.md)
3. [keccak256.c](https://github.com/firefly/wallet/blob/master/source/libs/ethers/src/keccak256.c), [keccak256.h](https://github.com/firefly/wallet/blob/master/source/libs/ethers/src/keccak256.h)

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
