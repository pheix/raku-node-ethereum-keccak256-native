#!/usr/bin/env raku
use v6.d;
use LibraryMake;

my %vars = get-vars('.');
%vars<keccak256> = $*VM.platform-library-name('keccak256'.IO);
mkdir "resources" unless "resources".IO.e;
mkdir "resources/libraries" unless "resources/libraries".IO.e;
process-makefile('.', %vars);
shell(%vars<MAKE>);
