unit class Node::Ethereum::Keccak256::Native;

use NativeCall;

constant KECCAK256 = %?RESOURCES<libraries/keccak256>;

class SHA3_CTX is repr('CStruct') {
    HAS uint64 @.hash[25] is CArray;
    HAS uint64 @.message[24] is CArray;
    has uint64 $.rest;
}

multi method keccak256(Str :$msg) returns buf8 {
    return self!keccak256_hash(:msgbuf(buf8.new($msg.encode)));
}

multi method keccak256(buf8 :$msg) returns buf8 {
    return self!keccak256_hash(:msgbuf($msg));
}

method !keccak256_hash(buf8 :$msgbuf) returns buf8 {
    my $msgcarr = CArray[uint8].new($msgbuf.list);
    my $outbuf  = CArray[uint8].new(0 xx 32);

    my $ctx    = CArray[uint64].new(0 xx 50);
    my $ctxptr = nativecast(Pointer[SHA3_CTX], $ctx);

    keccak_init($ctxptr);
    keccak_update($ctxptr, $msgcarr, $msgbuf.bytes);
    keccak_final($ctxptr, $outbuf);

    return buf8.new($outbuf);
}

sub keccak_init(Pointer $ctx) is native(KECCAK256) {*}
sub keccak_update(Pointer $ctx, CArray[uint8] $msg, uint16 $size) is native(KECCAK256) {*}
sub keccak_final(Pointer $ctx, CArray[uint8] $result) is native(KECCAK256) {*}
